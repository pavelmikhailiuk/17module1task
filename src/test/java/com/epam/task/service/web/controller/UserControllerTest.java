package com.epam.task.service.web.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.sun.jersey.multipart.impl.MultiPartWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Pavel on 10/30/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {
    private static final String API_URL = "http://localhost:8080/v1/users";
    private static final String USER_ID_1 = "/1";
    private static final String UPLOAD = "/logo/upload";
    private static final String DOWNLOAD = "/logo/download";
    private static final String USER_JSON = "{\"id\":2,\"firstName\":\"Jack\",\"lastName\":\"Daniels\",\"login\":\"jackie\",\"email\":\"jd@gmail.com\"}";
    private static final String USER_UPDATE_JSON = "{\"id\":2,\"firstName\":\"John\",\"lastName\":\"Connor\",\"login\":\"johnie\",\"email\":\"jc@gmail.com\"}";
    private static final String USER_1_XML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
            "<user>\n" +
            "    <id></id>\n" +
            "    <firstName>Jack</firstName>\n" +
            "    <lastName>Daniels</lastName>\n" +
            "    <login>jackie</login>\n" +
            "    <email>jd@gmail.com</email>\n" +
            "</user>";
    public static final String FILE_NAME_RAT_PNG = "rat.png";

    @Test
    public void testPostNewUser() throws Exception {
        Client client = Client.create();
        WebResource webResource = client
                .resource(API_URL);
        ClientResponse response = webResource.type(MediaType.APPLICATION_XML_TYPE)
                .post(ClientResponse.class, USER_1_XML);
        assertEquals(response.getStatus(), 201);
    }

    @Test
    public void testGetUserById() throws Exception {
        Client client = Client.create();
        WebResource webResource = client.resource(API_URL);
        ClientResponse response = webResource.type(MediaType.APPLICATION_XML_TYPE).post(ClientResponse.class, USER_1_XML);
        assertEquals(response.getStatus(), 201);
        List<String> location = response.getHeaders().get("Location");
        webResource = client.resource(location.get(0));
        response = webResource.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
        String expectedUser = response.getEntity(String.class);
        assertEquals(response.getStatus(), 200);
        assertEquals(expectedUser, USER_JSON);
    }

    @Test
    public void testUpdateUser() throws Exception {
        Client client = Client.create();
        WebResource webResource = client
                .resource(API_URL + USER_ID_1);
        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE)
                .put(ClientResponse.class, USER_UPDATE_JSON);
        assertEquals(response.getStatus(), 200);
    }

    @Test
    public void testDeleteUser() throws Exception {
        Client client = Client.create();
        WebResource webResource = client
                .resource(API_URL + USER_ID_1);
        ClientResponse response = webResource.delete(ClientResponse.class);
        assertEquals(response.getStatus(), 204);
    }

    @Test
    public void testUploadUserLogo() throws Exception {
        File logo = new File(FILE_NAME_RAT_PNG);
        FormDataMultiPart multiPart = new FormDataMultiPart();
        if (logo != null) {
            multiPart.bodyPart(new FileDataBodyPart("file", logo,
                    MediaType.APPLICATION_OCTET_STREAM_TYPE));
        }
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(MultiPartWriter.class);
        Client client = Client.create(config);
        WebResource webResource = client
                .resource(API_URL + USER_ID_1 + UPLOAD);
        ClientResponse response = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, multiPart);
        assertEquals(response.getStatus(), 201);
    }

    @Test
    public void testDownloadUserLogo() throws Exception {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource webResource = client
                .resource(API_URL + USER_ID_1 + DOWNLOAD);
        ClientResponse response = webResource.get(ClientResponse.class);
        File logo = response.getEntity(File.class);
        File file = new File(FILE_NAME_RAT_PNG);
        logo.renameTo(file);
        try (FileWriter fr = new FileWriter(logo)) {
            fr.flush();
        }
        assertEquals(response.getStatus(), 200);
    }
}
