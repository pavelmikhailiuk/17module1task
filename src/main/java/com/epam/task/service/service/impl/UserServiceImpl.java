package com.epam.task.service.service.impl;

import com.epam.task.service.domain.User;
import com.epam.task.service.exception.UserServiceException;
import com.epam.task.service.repository.UserRepository;
import com.epam.task.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Pavel_Mikhailiuk on 10/27/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void delete(Long id) throws UserServiceException {
        if (id == null) {
            throw new UserServiceException("Id is null");
        }
        try {
            userRepository.delete(id);
        } catch (Exception e) {
            throw new UserServiceException("Error delete user", e);
        }
    }

    @Override
    public User update(User entity) throws UserServiceException {
        if (entity == null) {
            throw new UserServiceException("User is null");
        }
        User user = null;
        try {
            user = userRepository.save(entity);
        } catch (Exception e) {
            throw new UserServiceException("Error update user", e);
        }
        return user;
    }

    @Override
    public Long save(User entity) throws UserServiceException {
        if (entity == null) {
            throw new UserServiceException("User is null");
        }
        Long id = null;
        try {
            User user = userRepository.save(entity);
            id = user != null ? user.getId() : null;
        } catch (Exception e) {
            throw new UserServiceException("Error save user", e);
        }
        return id;
    }

    @Override
    public User find(Long id) throws UserServiceException {
        if (id == null) {
            throw new UserServiceException("Id is null");
        }
        User user = null;
        try {
            user = userRepository.findOne(id);
        } catch (Exception e) {
            throw new UserServiceException("Error find user", e);
        }
        return user;
    }
}
