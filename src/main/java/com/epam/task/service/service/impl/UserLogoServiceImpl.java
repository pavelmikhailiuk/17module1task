package com.epam.task.service.service.impl;

import com.epam.task.service.domain.UserLogo;
import com.epam.task.service.exception.UserServiceException;
import com.epam.task.service.repository.UserLogoRepository;
import com.epam.task.service.service.UserLogoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLogoServiceImpl implements UserLogoService {
    @Autowired
    private UserLogoRepository userLogoRepository;

    @Override
    public void delete(Long id) throws UserServiceException {
        if (id == null) {
            throw new UserServiceException("Id is null");
        }
        try {
            userLogoRepository.delete(id);
        } catch (Exception e) {
            throw new UserServiceException("Error delete user logo", e);
        }
    }

    @Override
    public UserLogo update(UserLogo entity) throws UserServiceException {
        if (entity == null) {
            throw new UserServiceException("User logo is null");
        }
        UserLogo userLogo = null;
        try {
            userLogo = userLogoRepository.save(entity);
        } catch (Exception e) {
            throw new UserServiceException("Error update user logo", e);
        }
        return userLogo;
    }

    @Override
    public Long save(UserLogo entity) throws UserServiceException {
        if (entity == null) {
            throw new UserServiceException("User logo is null");
        }
        Long id = null;
        try {
            UserLogo userLogo = userLogoRepository.save(entity);
            id = userLogo != null ? userLogo.getId() : null;
        } catch (Exception e) {
            throw new UserServiceException("Error save user logo", e);
        }
        return id;
    }

    @Override
    public UserLogo find(Long id) throws UserServiceException {
        if (id == null) {
            throw new UserServiceException("Id is null");
        }
        UserLogo userLogo = null;
        try {
            userLogo = userLogoRepository.findOne(id);
        } catch (Exception e) {
            throw new UserServiceException("Error find user logo", e);
        }
        return userLogo;
    }

    @Override
    public UserLogo findByUserId(Long userId) throws UserServiceException {
        if (userId == null) {
            throw new UserServiceException("userId is null");
        }
        UserLogo userLogo = null;
        try {
            userLogo = userLogoRepository.findByUserId(userId);
        } catch (Exception e) {
            throw new UserServiceException("Error find user logo by userId", e);
        }
        return userLogo;
    }
}
