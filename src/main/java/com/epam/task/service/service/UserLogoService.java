package com.epam.task.service.service;

import com.epam.task.service.domain.UserLogo;
import com.epam.task.service.exception.UserServiceException;

public interface UserLogoService extends CrudService<UserLogo> {
    UserLogo findByUserId(Long userId) throws UserServiceException;
}
