package com.epam.task.service.service;

import com.epam.task.service.exception.UserServiceException;

public interface CrudService<T> {
    void delete(Long id) throws UserServiceException;

    T update(T entity) throws UserServiceException;

    Long save(T entity) throws UserServiceException;

    T find(Long id) throws UserServiceException;
}
