package com.epam.task.service.service;

import com.epam.task.service.domain.User;

/**
 * Created by Pavel_Mikhailiuk on 10/26/2016.
 */
public interface UserService extends CrudService<User> {
}
