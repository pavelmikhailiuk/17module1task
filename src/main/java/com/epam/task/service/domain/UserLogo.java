package com.epam.task.service.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;

@Entity
public class UserLogo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    private String name;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private byte[] data;

    public UserLogo(Long userId, String name, byte[] data) {
        this.userId = userId;
        this.name = name;
        this.data = data;
    }

    public UserLogo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLogo userLogo = (UserLogo) o;

        if (id != null ? !id.equals(userLogo.id) : userLogo.id != null) return false;
        if (userId != null ? !userId.equals(userLogo.userId) : userLogo.userId != null) return false;
        if (name != null ? !name.equals(userLogo.name) : userLogo.name != null) return false;
        return Arrays.equals(data, userLogo.data);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        return "UserLogo{" +
                "id=" + id +
                ", userId=" + userId +
                ", name='" + name + '\'' +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
