package com.epam.task.service.repository;

import com.epam.task.service.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel_Mikhailiuk on 10/26/2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
