package com.epam.task.service.repository;

import com.epam.task.service.domain.UserLogo;
import org.springframework.data.repository.CrudRepository;

public interface UserLogoRepository extends CrudRepository<UserLogo, Long> {
    UserLogo findByUserId(Long userId);
}
