package com.epam.task.service.web.exception;

import com.epam.task.service.exception.UserServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionController.class);

    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        LOGGER.error(ex.toString());
        response.sendError(HttpStatus.BAD_REQUEST.value(),
                "Sorry, something is going wrong, check your request or url please");
    }

    @ExceptionHandler(UserServiceException.class)
    public void handleUserServiceException(UserServiceException ex, HttpServletResponse response) throws IOException {
        LOGGER.error(ex.toString());
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Sorry, there was unexpected server error, check logs please");
    }
}