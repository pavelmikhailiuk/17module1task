package com.epam.task.service.web.controller;

import com.epam.task.service.domain.User;
import com.epam.task.service.domain.UserLogo;
import com.epam.task.service.exception.UserServiceException;
import com.epam.task.service.service.UserLogoService;
import com.epam.task.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Pavel_Mikhailiuk on 10/28/2016.
 */
@Controller
@RequestMapping(value = "/v1/users")
public class UserController {
    private static final String UPLOAD = "/logo/upload";
    private static final String DOWNLOAD = "/logo/download";
    private static final String USER_ID = "/{userId}";
    private static final String LOCATION_HEADER = "Location";
    private static final String V1_USERS = "/v1/users";

    @Autowired
    private UserService userService;

    @Autowired
    private UserLogoService userLogoService;

    @PostMapping(value = USER_ID + UPLOAD)
    public void uploadLogo(@RequestParam(required = true) MultipartFile file, @PathVariable Long userId,
                           HttpServletRequest request, HttpServletResponse response) throws UserServiceException, IOException {
        UserLogo userLogo = new UserLogo(userId, file.getOriginalFilename(), file.getBytes());
        Long id = userLogoService.save(userLogo);
        String url = request.getRequestURL().toString();
        response.addHeader(LOCATION_HEADER, url.substring(0, url.length() - (UPLOAD.length() - 1)) + id);
        response.setStatus(HttpStatus.CREATED.value());
    }


    @GetMapping(value = USER_ID + DOWNLOAD)
    public ResponseEntity<byte[]> downloadLogo(@PathVariable Long userId) throws UserServiceException {
        UserLogo userLogo = userLogoService.findByUserId(userId);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentDispositionFormData("attachment", userLogo.getName());
        return new ResponseEntity<>(userLogo.getData(), httpHeaders, HttpStatus.OK);
    }

    @GetMapping(value = USER_ID)
    public ResponseEntity<User> findUser(@PathVariable Long userId) throws UserServiceException {
        User user = userService.find(userId);
        return user == null ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
                : new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody @Validated User user, UriComponentsBuilder ucBuilder)
            throws UserServiceException {
        Long id = userService.save(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path(V1_USERS).path(USER_ID).buildAndExpand(id).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping(value = USER_ID)
    public ResponseEntity<User> updateUser(@RequestBody @Validated User user)
            throws UserServiceException {
        user = userService.update(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = USER_ID)
    public ResponseEntity<User> deleteUser(@PathVariable Long userId) throws UserServiceException {
        userService.delete(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
