package com.epam.task.service.exception;

public class UserServiceException extends Exception {
    public UserServiceException() {
    }

    public UserServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserServiceException(String message) {
        super(message);
    }

}
